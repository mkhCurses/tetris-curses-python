import curses

from curses import panel

class PanelWrapper:

    def __init__(self, stdscreen):
        self.stdscreen = stdscreen
        self.window = stdscreen.subwin(0, 0)
        self.window.keypad(1)
        self.panel = panel.new_panel(self.window)
        self.panel.hide()
        panel.update_panels()

    def prepare(self):
        self.panel.top()
        self.panel.show()
        self.window.clear()

    def release(self):
       self.window.clear()
       self.panel.hide()
       curses.doupdate()
