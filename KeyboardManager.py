#!/usr/bin/env python3

from threading import Thread

class KeyboardManager():

    def detectKeyPress(self, stdscreen):
        while(True):
            ch = stdscreen.getch()
            if self.listener != None:
                self.listener(ch)

    def startGetChThread(self, stdscreen):
        thread = Thread(target = self.detectKeyPress, args=[stdscreen])
        thread.daemon = True
        thread.start()

    def setListener(self, listener):
        self.listener = listener
