import queue
import curses
import random
from TFigure import TFigure
from TScreen import TScreen
import constants

class TGame:

    def __init__(self, stdscreen):
        self.pressedKeysQueue = queue.Queue()
        self.tick = 0
        self.screen = TScreen(stdscreen)
        self.figure = TFigure(self.screen)
        self.figure.shape(random.choice(constants.SHAPES))
        self.figure.pos(int(constants.FLD_WIDTH / 2 - constants.SHP_WIDTH / 2), 0)

    def prepare(self):
        self.screen.prepare()

    def release(self):
        self.screen.release()

    def show(self):
        self.screen.clear()
        self.figure.put()
        self.screen.show()

    def addKeyPressToQueue(self, ch):
        self.pressedKeysQueue.put(ch)

    def handlePressed(self, ch):
        key = chr(ch)
        if key == 's' or key == "S" or ch == curses.KEY_DOWN:
            self.figure.move(0, 1)
        elif key == 'a' or key == 'A' or ch == curses.KEY_LEFT:
            self.figure.move(-1, 0)
        elif key == 'd' or key == 'D' or ch == curses.KEY_RIGHT:
            self.figure.move(1, 0)
        elif key == 'w' or key == 'W' or ch == curses.KEY_UP:
            self.figure.setTurn(self.figure.turn + 1)

    def handlePressedKeys(self):
       while True:
           try:
               ch = self.pressedKeysQueue.get(False)
           except queue.Empty:
               break

           self.handlePressed(ch)

    def writeRecordToFile(self):
        win = self.screen.burned
        f = open("record.txt", "r")
        try:
            oldRecord = int(f.read())
        except ValueError:
            oldRecord = 0
        finally:
            f.close()

        f = open("record.txt", "w")
        if win > oldRecord:
            f.write(str(win))
        else:
            f.write(str(oldRecord))
        f.close()

    def move(self):
       self.tick += 1
       if self.tick >= 5:
           if not self.figure.move(0, 1):
               self.figure.putStatic()
               self.figure.shape(random.choice(constants.SHAPES))
               self.figure.pos(constants.FLD_WIDTH / 2 - constants.SHP_WIDTH / 2, 0)
               if self.figure.check() > 0:
                   self.writeRecordToFile()
                   self.screen.refreshRecord()
                   self.screen.restart()
                   self.screen.incrementLose()

           self.screen.burning()
           self.tick = 0
