from PaintType import PaintType

FLD_WIDTH = 20
FLD_HEIGHT = 30
SCR_WIDTH = FLD_WIDTH * 2
SCR_HEIGHT = FLD_HEIGHT

SHP_WIDTH = 4
SHP_HEIGHT = 4

SHAPES = (
    (
        (PaintType.EMPTY, PaintType.EMPTY,PaintType.EMPTY,PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY),
        (PaintType.EMPTY,PaintType.EMPTY,PaintType.EMPTY,PaintType.EMPTY,)
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY)
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY),
        (PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY)
    ),
    (
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.HIGHLIGHTED, PaintType.HIGHLIGHTED, PaintType.EMPTY),
        (PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY, PaintType.EMPTY)
    )
)
