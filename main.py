#!/usr/bin/env python3

from KeyboardManager import KeyboardManager
import subprocess
import threading
import time
import curses
from TGame import TGame

class App(object):

    def prepare(self, stdscreen):
        self.stdscreen = stdscreen
        self.isGamePaused = False
        self.isQuitPressed = False
        self.game = TGame(stdscreen)

    def startGame(self):
        self.game.prepare()

        keyManager = KeyboardManager()
        keyManager.setListener(self.keyListener)
        keyManager.startGetChThread(self.stdscreen)

        while True:
            if self.isQuitPressed:
                break

            if self.isGamePaused:
                self.game.show()
            else:
                self.game.show()
                self.game.handlePressedKeys()
                self.game.move()
            time.sleep(0.05)

        self.game.release()

    def writeRecordToFile(self):
        if self.game != None:
            self.game.writeRecordToFile()

    def keyListener(self, ch):
        key = chr(ch)
        if key == 'p' or key == 'P':
            self.isGamePaused = not self.isGamePaused

        if key == 'q' or key == 'Q':
            self.isQuitPressed = True

        if not self.isGamePaused:
            self.game.addKeyPressToQueue(ch)

    def startAudioThread(self):
        thread = threading.Thread(target = self.playAudio)
        thread.daemon = True
        thread.start()

    def playAudio(self):
        try:
            p = subprocess.Popen(
                ["mpg123", "audio.mp3"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            p.communicate()
            if p.returncode == 0:
                self.startAudioThread()
        except FileNotFoundError:
            pass

def launch(stdscreen, app):
    curses.curs_set(0)
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_CYAN)
    curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_YELLOW)
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)
    curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_GREEN)

    open("record.txt", "a").close()

    app.prepare(stdscreen)
    app.startAudioThread()
    app.startGame()

if __name__ == "__main__":
    app = None
    try:
        app = App()
        curses.wrapper(launch, app)
    except KeyboardInterrupt:
        if app != None:
            app.writeRecordToFile()
        exit()
