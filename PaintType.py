from enum import Enum

class PaintType(Enum):
    EMPTY = 0
    HIGHLIGHTED = 1
    HIGHLIGHTED_ASTERISK = 2
