import curses
import constants
from PanelWrapper import PanelWrapper
from PaintType import PaintType

import random
import string

class TScreen(PanelWrapper):

    def __init__(self, stdscreen):
        super().__init__(stdscreen)
        self.record = 0
        self.burned = 0
        self.losed = 0
        self.burnedTick = 0
        self.losedTick = 0
        self.recordTick = 0
        self.screen_map = [[PaintType.EMPTY] * constants.SCR_WIDTH] * constants.SCR_HEIGHT
        self.refreshRecord()

    def restart(self):
        del self.screen_map[:]
        self.screen_map.extend([[PaintType.EMPTY] * constants.SCR_WIDTH] * constants.SCR_HEIGHT)

    def incrementLose(self):
        self.losedTick = 0
        self.losed += 1
        self.burned = 0
        self.burnedTick = 0

    def refreshRecord(self):
        f = open("record.txt", "r")
        try:
            newRecord = int(f.read())
        except ValueError:
            newRecord = 0
        finally:
            f.close()

        if newRecord > self.record:
            self.record = newRecord
            self.recordTick = 0

    def clear(self):
        result = []
        for i in range(0, constants.SCR_HEIGHT):
            row = []
            for j in range(0, constants.SCR_WIDTH):
                if self.screen_map[i][j] == PaintType.HIGHLIGHTED_ASTERISK:
                    row.append(PaintType.HIGHLIGHTED_ASTERISK)
                else:
                    row.append(PaintType.EMPTY)

            result.append(row)

        del self.screen_map[:]
        self.screen_map.extend(result)

    def show(self):
        self.window.clear()
        for y in range(0, constants.SCR_HEIGHT):
            for x in range(0, constants.SCR_WIDTH):
                paintType = self.screen_map[y][x]

                ch = None
                mode = None

                if paintType == PaintType.EMPTY:
                    ch = random.choice(string.ascii_lowercase)
                    mode = curses.color_pair(1)

                elif paintType == PaintType.HIGHLIGHTED:
                    ch = ' '
                    mode = curses.color_pair(2)

                elif paintType == PaintType.HIGHLIGHTED_ASTERISK:
                    ch = '*'
                    mode = curses.color_pair(3)

                self.window.addch(y, x, ch, mode)

        if self.losedTick < 10:
            self.losedTick += 1
            losedMode = curses.color_pair(3)
        else:
            losedMode = curses.A_STANDOUT

        if self.burnedTick < 10:
            self.burnedTick +=1
            burnedMode = curses.color_pair(4)
        else:
            burnedMode = curses.A_STANDOUT

        if self.recordTick < 10:
            self.recordTick +=1
            recordMode = curses.color_pair(4)
        else:
            recordMode = curses.A_STANDOUT


        self.window.addstr(1, constants.SCR_WIDTH + 2, "Record: %d" % self.record, recordMode)
        self.window.addstr(3, constants.SCR_WIDTH + 2, "Win: %d" % self.burned, burnedMode)
        self.window.addstr(5, constants.SCR_WIDTH + 2, "Lose: %d" % self.losed, losedMode)

        self.window.refresh()

    def burning(self):
        filledLines = []
        for i in range(constants.SCR_HEIGHT - 1, -1, -1):
            fillLine = True

            for j in range(0, constants.SCR_WIDTH):
                if self.screen_map[i][j] != PaintType.HIGHLIGHTED_ASTERISK:
                    fillLine = False
                    break

            if fillLine:
                filledLines.append(i)

        filledLinesLen = len(filledLines)
        if filledLinesLen != 0:
            self.burned += filledLinesLen
            self.burnedTick = 0

            for l in filledLines:
                del self.screen_map[l]

            for l in filledLines:
                self.screen_map.insert(0, [PaintType.EMPTY] * constants.SCR_WIDTH)
