import constants

from PaintType import PaintType

class TFigure:

    def __init__(self, screen):
        self.screen = screen
        self.coord = []
        for _ in range(0, (constants.SCR_WIDTH * constants.SCR_HEIGHT)):
            self.coord.append((0, 0))
        self.cordCnt = 0
        self.turn = 0

    def shape(self, vid):
        self.vid = vid

    def pos(self, x, y):
        self.x = int(x)
        self.y = int(y)
        self.calcCord()

    def put(self):
        for i in range(self.cordCnt):
            self.screen.screen_map[self.coord[i][1]][int(self.coord[i][0] * 2)] = self.screen.screen_map[self.coord[i][1]][int(self.coord[i][0] * 2 + 1)] = PaintType.HIGHLIGHTED

    def putStatic(self):
        for i in range(self.cordCnt):
            self.screen.screen_map[self.coord[i][1]][int(self.coord[i][0] * 2)] = self.screen.screen_map[self.coord[i][1]][int(self.coord[i][0] * 2 + 1)] = PaintType.HIGHLIGHTED_ASTERISK

    def move(self, dx, dy):
       oldX = self.x
       oldY = self.y
       self.pos(self.x + dx, self.y + dy)
       chk = self.check()
       if chk >= 1:
           self.pos(oldX, oldY)
           if chk == 2:
               return False
       return True

    def calcCord(self):
        self.cordCnt = 0
        for i in range(0, constants.SHP_WIDTH):
            for j in range(0, constants.SHP_HEIGHT):
                if self.vid[j][i] == PaintType.HIGHLIGHTED:
                    if self.turn == 0:
                        xx = self.x + i
                        yy = self.y + j
                    elif self.turn == 1:
                        xx = self.x + (constants.SHP_HEIGHT - j - 1)
                        yy = self.y + i
                    elif self.turn == 2:
                        xx = self.x + (constants.SHP_WIDTH - i - 1)
                        yy = self.y + (constants.SHP_HEIGHT - j -1)
                    else:
                        xx = self.x + j
                        yy = self.y + (constants.SHP_HEIGHT - i -1) + (constants.SHP_WIDTH - constants.SHP_HEIGHT)

                    self.coord[self.cordCnt] = (xx, yy)
                    self.cordCnt += 1

    def check(self):
        self.calcCord()
        for i in range(0, self.cordCnt):
            if self.coord[i][0] < 0 or self.coord[i][0] >= constants.FLD_WIDTH:
                return 1

        for i in range(0, self.cordCnt):
            if self.coord[i][1] >= constants.FLD_HEIGHT or self.screen.screen_map[self.coord[i][1]][self.coord[i][0] * 2] == PaintType.HIGHLIGHTED_ASTERISK or self.screen.screen_map[self.coord[i][1]][self.coord[i][0] * 2 + 1] == PaintType.HIGHLIGHTED_ASTERISK:
                return 2

        return 0

    def setTurn(self, _turn):
        oldTurn = self.turn
        if _turn > 3:
            self.turn = 0
        elif _turn < 0:
            self.turn = 3
        else:
            self.turn = _turn

        chk = self.check()
        if chk == 0:
            return
        if chk == 1:
            xx = self.x
            if self.x > constants.FLD_WIDTH / 2:
                k = -1
            else:
                k = +1

            for _ in range(1, 3):
                self.x += k
                if self.check() == 0:
                    return
            self.x = xx

        self.turn = oldTurn
        self.calcCord()
